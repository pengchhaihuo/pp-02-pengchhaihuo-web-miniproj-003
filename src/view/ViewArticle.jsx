import { Container, Row, Col } from 'react-bootstrap'
import React ,{useEffect, useState} from 'react'
import { useParams } from 'react-router'
import { useSelector } from 'react-redux'
import MyComponent from '../components/MyComponent'


export default function ViewArticle() {

    const article = useSelector(state => state.articleReducer.article)

    let param = useParams()

    const [articleView, setArticleView] = useState([])

    useEffect(() => {
        setArticleView(article.filter((item) => item._id == param.id))
    }, [])
    console.log(param.id);
console.log("articleView",articleView);
    

    return (
        <Container>
            <Row>
                <Col className="md-7">
                   {articleView.map(
                       (item, index)=>(
                           <div key={index}>
                        <h3>{item.title}</h3>
                        <img src={item.image} alt="image" width="500px" />
                        <p>{item.description}</p>
                         </div>
                       )
                   )}
                </Col>
                <Col>
                    <h2>Comment</h2>
                    <MyComponent/>
                </Col>
            </Row>
        </Container>
    )
}
