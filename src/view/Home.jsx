import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useEffect } from 'react'
import { onDeleteArticle, onGetArticle } from '../redux/action/aritcleAction';
import { Card, Button, Container, Row, Col } from 'react-bootstrap'
import { onGetCategory } from '../redux/action/categoryAction';
import { bindActionCreators } from 'redux';
import { useHistory } from 'react-router';

export default function Home() {

const history = useHistory();

    const article = useSelector(state => state.articleReducer.article)
    const category = useSelector(state => state.categoryReducer.category)
    const dispatch = useDispatch();
    const onDelete = bindActionCreators(onDeleteArticle,dispatch)

    useEffect(() => {
        dispatch(onGetArticle())
        dispatch(onGetCategory())
    }, [])

    console.log(category);
    console.log(article);
    return (
        <Container>
            <Row >
                <Col md="3" className="py-2">
                    <Card >
                        <Card.Img variant="top" src="images/ben10.jpg" width="60px" />
                        <Card.Body>
                            <Card.Text>Please Login
                            </Card.Text>
                            <Button variant="primary" size="sm">Login with Google</Button>
                        </Card.Body>
                    </Card>
                </Col>

                <Col>
                    <h3>Category</h3>
                    <Row>
                        {
                            category.map(
                                (item, index) => (
                                    <Button className="mx-1 shadow-sm rounded" key={index}  variant="light" size="sm">{item.name}</Button>                            )
                            )
                        }
                    </Row>

                    <Row>
                        {
                            article.map(
                                (item, index) => (
                                    <Col className="py-2 px-1" md={3} key={index}>
                                        <Card className="shadow rounded">
                                            <Card.Img variant="top" src={item.image} style={{ objectFit: "cover", height: "150px" }} />
                                            <Card.Body >
                                                <Card.Title>{item.title}</Card.Title>
                                                <Card.Text>{item.description}
                                                </Card.Text>
                                                <div className="text-center">
                                                    <Button variant="primary" size="sm" onClick={()=>history.push("/article/"+item._id)}>Read</Button>{" "}
                                                    <Button variant="warning" size="sm">Edit</Button>{" "}
                                                    <Button variant="danger" size="sm" onClick={()=>onDelete(item._id)  } >Delete</Button>
                                                </div>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                )
                            )
                        }
                    </Row>
                </Col>
            </Row>

        </Container>
    )
}
