import React from 'react'
import { Form, Button, Container, Row, Col } from 'react-bootstrap'
import {  useState } from 'react'
import { onPostArticle } from '../redux/action/aritcleAction'
import { useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import Swal from 'sweetalert2'

export default function Article() {
    
    // const [articles, setArticles] = useState([])
    // const [imageFile, setImageFile] = useState()
    const [imageUrl, setimageUrl] = useState("/images/author-icon.jpg")
    const [title, setTitle] = useState("")
    const [description, setDescription] = useState("")



    const dispatch = useDispatch();
    const onPost = bindActionCreators(onPostArticle,dispatch)

    //postImageToApi(imageFile);
    const onSubmit = async (e) => {
        e.preventDefault()
        // const urlForAPI = await postImageToApi(imageFile)
        // console.log(urlForAPI);
        const postArticle = {
            title,
            description,   // write email(shortcut) only because it has same name between api and states
            // image: urlForAPI
        }
        console.log(postArticle);
         onPost(postArticle).then(
             message =>{
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: message.payload,
                    showConfirmButton: false,
                    timer: 2000
                  })
             }
            
         )
        // clearForm()
    }


    // const onGetImageInfo = (e) => {
    //     console.log(e.target.files[0]);
    //     let url = URL.createObjectURL(e.target.files[0])
    //     setimageUrl(url)
    //     setImageFile(e.target.files[0])
    // }
//  const results = await fetchAllData()
    // useEffect(async () => {
        
    //     setArticles(await fetchAllData())
    //     console.log(articles);
    //      setRefreshTable(false)
    // }, [refreshTable])

    const clearForm = ()=>{
        setTitle("")
        setDescription("")
        setimageUrl("/images/author-icon.jpg")
    }

    // const onDeleteAuthor = async (id)=>{
    //    const result =  await deleteAuthor(id)
    //     setRefreshTable(true)
    // }


    return (
        <Container>
        <h2>Author</h2>
        <Row>
            <Col md={8}>
                <Form>
                    <Form.Group >
                        <Form.Label>Title</Form.Label>
                        <Form.Control
                            value={title}
                            onChange={(e) => { setTitle(e.target.value) }}
                            type="text"
                            placeholder="Author Name" />
                    </Form.Group>
                    <Form.Group >
                        <Form.Label>Description</Form.Label>
                        <Form.Control
                            value={description}
                            onChange={(e) => { setDescription(e.target.value) }}
                            type="text"
                            placeholder="Author Name" />
                    </Form.Group>
                    <Button
                    type="submit"
                        onClick={onSubmit}
                        variant="primary" >Add</Button>
                </Form>
            </Col>
            <Col>
                <img src={imageUrl} width="256px" />
                <Form.Group controlId="formFileSm" className="mb-3">
                    <Form.Control
                        // onChange={
                        //     (e) => onGetImageInfo(e)}
                        type="file" size="sm" />
                </Form.Group>
            </Col>
        </Row>
    </Container>
    )
}
