import { articleActionType } from "../../action/articleActionType";


const initialState = {
    article: [],
};

export default function articleReducer(state=initialState, action){
    switch(action.type){
        case articleActionType.GET_ARTICLE:
            return { ...state, article: [...action.payload]}
        case articleActionType.DELETE_ARTICLE:
            return { ...state, article: state.article.filter(item=>item._id !== action.payload._id)}
        case articleActionType.INSERT_ARTICLE:
            return { ...state, action}
        default: 
            return state;
    }
}