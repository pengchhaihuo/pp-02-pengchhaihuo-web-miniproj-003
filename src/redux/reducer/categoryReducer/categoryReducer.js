
import { categoryActionType } from "../../action/categoryActionType";


const initialState = {
    category: [],
};

export default function categoryReducer(state=initialState, action){
    switch(action.type){
        case categoryActionType.GET_CATEGORY:
            return { ...state, category: [...action.payload]}
        default: 
            return state;
    }
}