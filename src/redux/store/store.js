
import {applyMiddleware, combineReducers, createStore} from 'redux'
import articleReducer from '../reducer/articleReducer/articleReducer'
import categoryReducer from '../reducer/categoryReducer/categoryReducer'
import thunk from 'redux-thunk'

const rootReducer = combineReducers(
    {
        articleReducer,
        categoryReducer,
    }
)

export const store = createStore(rootReducer, applyMiddleware(thunk))