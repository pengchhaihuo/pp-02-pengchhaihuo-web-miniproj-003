
import { API } from "../../services/api/API";
// import { get_all_article, post_article } from "../../services/articleService"
import { categoryActionType } from "./categoryActionType";

export const onGetCategory = ()=> async (dispatch)=>{
    // let article = await get_all_article();
    const result = await API.get("/category")
    return dispatch(
        {   
            type: categoryActionType.GET_CATEGORY,
            payload: result.data.data
        }
    )
};