import { API } from "../../services/api/API";
// import { get_all_article, post_article } from "../../services/articleService"
import { articleActionType } from "./articleActionType";

export const onGetArticle = ()=> async (dispatch)=>{
    // let article = await get_all_article();
    const result = await API.get("/articles")
    return dispatch(
        {
            type: articleActionType.GET_ARTICLE,
            payload: result.data.data,
        }
    )
};

export const onPostArticle = (article)=> async (dispatch)=>{
    const result = await API.post("/articles", article)
    return dispatch(
        {
            type: articleActionType.INSERT_ARTICLE,
            payload: result.data.message,
        }
    )
};

export const onDeleteArticle = (id)=> async (dispatch)=>{
    const result = await API.delete(`/articles/${id}`)
    return dispatch(
        {
            type: articleActionType.DELETE_ARTICLE,
            payload: result.data.data,
        }
    )
};
