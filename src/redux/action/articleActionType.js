
export const articleActionType={
    GET_ARTICLE : 'GET_ARTICLE',
    INSERT_ARTICLE : 'INSERT_ARTICLE',
    UPDATE_ARTICLE : 'UPDATE_ARTICLE',
    DELETE_ARTICLE : 'DELETE_ARTICLE',
}