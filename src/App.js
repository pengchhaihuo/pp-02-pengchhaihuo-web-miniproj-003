import React from 'react'
import Menu from './components/Menu'
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Home from './view/Home';
import Article from './view/Article';
import Author from './view/Author';
import Category from './view/Category';
import ViewArticle from './view/ViewArticle';

export default function App() {
  return (
    <Router>
      <Menu/>
      <Switch>
        <Route path="/home" component = {Home}/>
        <Route exact path="/article" component={Article}/>
        <Route path="/article/:id" component={ViewArticle}/>
        <Route path="/author" component={Author} />
        <Route path="/category" component={Category}/>
      </Switch>
    </Router>
  )
}
