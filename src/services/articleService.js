import { API } from "./api/API"

export const get_all_article = async ()=>{
    try{
        const result = await API.get("/articles")
        return result.data.data
    }catch(err){
        alert(err)
    }
}

export const post_article = async (article)=>{
    try{
        const result = await API.post("/articles",article)
        return result.data.data
    }catch(err){
        alert(err)
    }
}
export const delete_article = async (id)=>{
    try{
        const result = await API.delete(`/articles/${id}`)
        return result.data.message
    }catch(err){
        alert(err)
    }
}

export const update_article = async (id, articleForUpdate)=>{
        try{
            const result = await API.put(`/articles/${id}`, articleForUpdate)
            return result.data.message
        }catch(err){
            alert(err)
        }
}

